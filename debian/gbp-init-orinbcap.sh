#!/bin/sh

# gbp-init-orinbcap

# Generate a orinbcap Debian package of the '3.0 (quilt)' kind from nothing.
#
# I wrote this script because I needed a way to automate the finicky process of
# "Debianizing" the https://github.com/DENSORobot/orin_bcap project. I could
# have done this all manually, but, having done that before, the git history
# ends up looking like hot garbage. This way, the git history is kept clean, or
# at least cleaner than it would have otherwise. The result is subsequently
# easier to work with and makes it look like I got it right the first time. :D
#
# -- neil 2022.10.24

this="$(realpath "$0")"
readonly this="${this}"

set -euvx

# Die unless some environment variables are already defined.
test -n "${DEBFULLNAME}"
test -n "${DEBEMAIL}"

# Some of the git commands I use for diagnostics implicitly page enerything
# through 'more' which requires interacting with the terminal. Override PAGER
# to prevent that.
export PAGER=cat

export PKGNAME="orinbcap"

# This can be anything, but it will be created afresh and emptied with every
# invocation of this script.
PKGDIR="${HOME}"/tmp/"${PKGNAME}"
mkdir -vp "${PKGDIR}"
find "${PKGDIR}" -mindepth 1 -delete

# Change into the package directory so we can use shorter relative paths.
cd "${PKGDIR}"

# Initialize $PWD as a git worktree.
git init .

# Add the remote named 'origin' pointing to the URI. We assume that the remote
# repository already exists.
git remote add origin https://gitlab.com/realtime-robotics/orinbcap.git

# Fetch anything and everything from the remote.
git fetch --all
git fetch --tags

# Delete all tags from the remote.
git tag --list | xargs -rI{} git push --verbose --delete origin {}

# Delete all tags from the local worktree.
git tag --list | xargs -rI{} git tag --delete {}

# Delete these branches from the remote.
git push --verbose --force --delete origin upstream || true
git push --verbose --force --delete origin pristine-tar || true

# Make an empty commit to the default branch 'master'.
git commit --allow-empty --message="initial commit"

# Force-push 'master' to the remote. This assumes that 'master' is unprotected.
git push --verbose --force --set-upstream origin master

# Fetch the things we just force-pushed
git fetch --all
git fetch --tags

################################################################################

#############################
# POPULATE DEBIAN DIRECTORY #
#############################

# First, create the directory.
mkdir -vp debian

# Add this script to the packaging. I figure this serves as a record of what I
# did in the beginning.
cp -v "${this}" debian

# Generate the debian/source/format file. This is crucial for identifying the
# kind of package being installed. Only '3.0 (quilt)' packages are allowed to
# import original tarballs.
mkdir -vp debian/source
cat >debian/source/format <<'EOF'
3.0 (quilt)
EOF

# Generate debian/control. This is used by dch (later in this script) to create
# the debian/changelog file. Specifically, dch reads this file to discover the
# Source package name.
#
# The major version 1 suffix on the 'Package: libkrnx1' must be known _a
# priori_. That is, this script assumes that we already know the version of the
# original tarball being packaged.
cat >debian/control <<'EOF'
Source: orinbcap
Section: devel
Priority: optional
Maintainer: Neil Roza <neil@rtr.ai>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 patchelf,
Standards-Version: 4.6.1
Homepage: https://github.com/DENSORobot/orin_bcap
Vcs-Browser: https://gitlab.com/realtime-robotics/orinbcap
Vcs-Git: https://gitlab.com/realtime-robotics/orinbcap.git

Package: liborinbcap-dev
Section: libdevel
Architecture: any
Depends:
 liborinbcap1 (= ${binary:Version}),
 ${misc:Depends},
Description: Denso Wave b-CAP C API (development files)
 Denso Wave b-CAP C API.
 .
 This package contains the development headers.

Package: liborinbcap1
Section: libs
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Denso Wave b-CAP C API (runtime files)
 Denso Wave b-CAP C API
 .
 This package contains the shared objects.
EOF

# Generate the debian/watch file. This is absolutely crucial for a correct
# invocation to 'gbp import-orig --uscan'.
#
# https://wiki.debian.org/debian/watch
cat >debian/watch <<'EOF'
version=4
opts=filenamemangle=s/.+\/v?(\d\S+)\.tar\.gz/orin_bcap-$1\.tar\.gz/ \
  https://github.com/DENSORobot/orin_bcap/tags .*/v?(\d\S+)\.tar\.gz
EOF

# Generate debian/copyright. This one is simple, but a more complicated one
# (with Files-Excluded directives) _could_ be used by uscan (as wrapped by
# gbp-import-orig) to modify the original tarball we acquire from upstream.
#
# The complete license texts are omitted for brevity. Lintian doesn't like
# this, but thankfully only flags this as a warning --- and not as a
# build-breaking error.
cat >debian/copyright <<'EOF'
Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: orin_bcap
Upstream-Contact:
 furukawa <hiromi.furukawa@denso-wave.co.jp>,
 Yoshihiro Miyakoshi <yoshihiro.miyakoshi@denso-wave.co.jp>,
Source: https://github.com/DENSORobot/orin_bcap

Files: *
Copyright: 2015-2022 Denso Wave Inc <info@denso-wave.co.jp>
License: MIT

Files: debian/*
Copyright: 2022 Neil Roza <neil@rtr.ai>
License: BSD-2-Clause
EOF

# Generate the debian/*.install files. These are standard boilerplate for the
# classic library topology; i.e., one binary package for the shared object and
# one binary package for the development headers.
cat >debian/liborinbcap-dev.install <<'EOF'
usr/include/*
usr/lib/*/*.so
EOF

cat >debian/liborinbcap1.install <<'EOF'
usr/lib/*/*.so.*
EOF

# Generate the debian/rules file.
cat >debian/rules <<'EOF'
#!/usr/bin/make -f

include /usr/share/dpkg/default.mk

export DH_VERBOSE = 1

%:
	dh $@ -v --parallel --buildsystem=cmake --builddirectory=build

# Why custom cmake options?
#
#   -S ../C
#
#     Because the things under the C directory are the only things we want to
#     build. We need to go "up" (../) one level because the
#     '--builddirectory=build' puts us in that working directory before
#     executing this target rule body.
#
#   -DBUILD_SHARED_LIBS=true
#
#     Because Denso would prefer to build static archives rather than shared
#     objects. This is not necessarily wrong, but it is weird and should be
#     avoided.
#
override_dh_auto_configure:
	dh_auto_configure -- -S ../C -DBUILD_SHARED_LIBS=true

EOF
chmod +x debian/rules

cat >debian/gbp.conf <<'EOF'
# orinbcap/debian/gbp.conf

[DEFAULT]
debian-tag = ubuntu/%(version)s
debian-tag-msg = %(pkg)s Ubuntu release %(version)s
pristine-tar = True
sign-tags = True
EOF

################################################################################

# Use 'dch' to create the 'debian/changelog' file. This invocation _should_ be
# able to discover the Source name from the 'debian/control' file but, for some
# reason, doesn't. Force the name by passing the '--package=krnx' option.
#
# This is an 'UNRELEASED' release with a fake version '0.0.0'.
dch --create --empty --package=orinbcap --newversion=0.0.0

# Add all the 'debian/*' contents to source control; commit the result.
git add debian
git commit --message="add debian"

################################################################################

#########################################
# IMPORT ORIGINAL TARBALL FROM UPSTREAM #
#########################################

# Create a local branch called 'upstream'. The subsequent invocation to
# 'gbp-import-orig' will fail if this branch is not present.
git branch upstream

# Invoke 'gbp-import-orig' verbosely, non-interactively, and delegate to
# 'uscan'. The execution of 'uscan' will read the 'debian/watch' file to
# automagically acquire the latest available tarball from the upstream
# repository.
gbp import-orig --verbose --no-interactive --uscan --pristine-tar

################################################################################

##################
# CREATE PATCHES #
##################

# This hack is specific to orin_bcap. The authors of C/CMakeLists.txt do some
# things I don't like:
#
# * The installation destination for the libraries is incompatible with modern
#   multiarch support.
#
# * The SONAME for each of the libraries is unversioned.
#
# * The libraries have no version-respecting symlinks.
#
# The purpose of this patch is to address all three deficiencies.
#
# Normally, the library VERSION and SOVERSION follow the version of the
# package. Unfortunately, as of this writing, the upstream developers do not
# declare any such information in source control. That is, if Denso had written
# 'C/CMakeLists.txt' with a
#
#     project(bcap_core VERSION 1.0.0)
#
# then I could have used that to populate VERSION and SOVERSION. If and when
# Denso declares a version, then the 'C/CMakeLists.txt' patch should be changed
# to consume it.

# Create the patch-queue branch and switch to it.
gbp pq import

# Patch 'C/CMakeLists.txt' accordingly.
git apply --verbose - <<'EOF'
diff --git a/C/CMakeLists.txt b/C/CMakeLists.txt
index b4422ca..cb44c91 100644
--- a/C/CMakeLists.txt
+++ b/C/CMakeLists.txt
@@ -212,10 +212,16 @@ else(_USE_catkin)
     )
   endif(WIN32 AND NOT BUILD_SHARED_LIBS)
 
+  set_target_properties(bcap_client bcap_server tpcomm rac_string
+    PROPERTIES
+    VERSION 1.0.0
+    SOVERSION 1
+  )
+  include(GNUInstallDirs)
   install(TARGETS bcap_client bcap_server tpcomm rac_string
-    ARCHIVE DESTINATION ${_TARGET_ARCHITECTURE}${_VS_VER}lib
-    LIBRARY DESTINATION ${_TARGET_ARCHITECTURE}${_VS_VER}lib
-    RUNTIME DESTINATION ${_TARGET_ARCHITECTURE}${_VS_VER}bin
+    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
+    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
+    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
     )
 
   # headers
@@ -227,4 +233,4 @@ else(_USE_catkin)
     PATTERN ".svn" EXCLUDE
   )
 
-endif(_USE_catkin)
\ No newline at end of file
+endif(_USE_catkin)
EOF

# The file is changed, but not yet added or committed. Add it and commit it.
git commit --all --message="edit C/CMakeLists.txt: fix version and install dest"

# Export the patch-queue branch to create the debian/patches directory and the
# files therein.
gbp pq export

################################################################################

######################
# CREATE THE RELEASE #
######################

# Parse the most recent tag to discover the upstream_version (assumed to be a
# dotted-triplet) of the original tarball just added by 'gbp-import-orig'.
upstream_version="$(
    git describe --abbrev=0 \
        | grep -Ex 'upstream/[[:digit:]]+[.][[:digit:]]+[.][[:digit:]]+' \
        | grep -Eo '[[:digit:]]+[.][[:digit:]]+[.][[:digit:]]+$'
)"

# Invoke 'gbp-dch' to create the first release of this package. Note the
# trailing '-1' revision number that denotes this as the first package
# maintainer release of the given upstream version.
gbp dch \
    --verbose \
    --release \
    --commit \
    --distribution=unstable \
    --new-version="${upstream_version}-1" \
    --spawn-editor=never

#######################################
# BUILD, TAG, AND PUBLISH THE RELEASE #
#######################################

# Invoke 'gbp-buildpackage' to build the previously-released package. Override
# the distribution via '--dist='; this assumes the gbp buildpackage builder is
# 'sbuild' or similar. If the build succeeds, then tag the result.
gbp buildpackage \
    --dist="$(. /etc/os-release && echo "${VERSION_CODENAME}")" \
    --git-no-hooks \
    --git-tag

# Push everything to the remote.
gbp push --verbose

exit "$?"
